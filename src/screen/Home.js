import React, { Component } from 'react';
import { Text, Image, View, FlatList, ActivityIndicator, StyleSheet } from 'react-native';
import Config from "react-native-config";
import { TouchableOpacity } from 'react-native-gesture-handler';

const axios = require('axios');
const moment = require('moment');


export default class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            newsData: [],
            page: 0,
        }
    }

    async loadNews(page) {
        let currPage = page + 1;
        console.log(currPage);
        axios.get(`${Config.API_URL}top-headlines`, {
            params: {
                page: currPage,
                apiKey: Config.API_KEY,
                country: 'id',
                pageSize: 20
            }
        }).then((result) => {
            const newsData = result.data.articles;

            this.setState({
                newsData: this.state.newsData.concat(newsData),
                isLoading: false,
                page: currPage
            })
        }).catch((error) => {
            alert(error);
        })
    }

    componentDidMount() {
        this.loadNews(this.state.page);
    }

    newsOnPress(url) {
        this.props.navigation.navigate('Detail', { url });
    }

    renderNews = ({ item }) => {
        return (
            <TouchableOpacity onPress={() => this.newsOnPress(item.url)} style={styles.containerNews}>
                <View style={styles.containerHeader}>
                    <Text style={styles.title}>{item.title}</Text>
                    <Image style={styles.image} source={{ uri: item.urlToImage }}/>
                </View>
                <Text style={[styles.text, {fontWeight: 'bold'}]}>{item.author}</Text>
                <Text style={styles.text}>{item.description}</Text>
                <Text style={[styles.text, {fontWeight: 'bold'}]}>{moment(item.publishedAt).format('MMMM Do YYYY, h:mm:ss a')}</Text>
            </TouchableOpacity>
        );
    }

    render() {
        const { isLoading, newsData } = this.state;

        if (isLoading) {
            return (
                <View style={styles.containerLoading}>
                    <ActivityIndicator size="large" color="tomato" />
                </View>
            );
        } else {
            return (
                <View style={styles.container}>
                    <FlatList
                        data={newsData}
                        renderItem={this.renderNews}
                        onEndReached={() => this.loadNews(this.state.page)}
                        keyExtractor={item => item.url}
                    />
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    containerLoading: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    container: {
        flex: 1,
    },
    containerHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 5,
    },
    containerNews: {
        padding: 10,
        borderWidth: StyleSheet.hairlineWidth,
        borderRadius: 15,
        margin: 10
    },
    title: {
        fontSize: 16,
        flex: 1,
        color: 'tomato',
        marginRight: 5
    },
    text: {
        fontSize: 12,
    },
    image: {
        width: 100,
        height: 100,
        borderRadius: 20
    }
})