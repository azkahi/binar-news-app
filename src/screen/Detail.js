import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { WebView } from 'react-native-webview';

export default class DetailScreen extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <WebView source={{ uri: this.props.route.params.url }} />
      </View>
    );
  }
}